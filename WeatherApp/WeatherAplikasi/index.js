import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Router from './Router'

export default function WeatherApp() {
    return (
        <Router />
    )
}

const styles = StyleSheet.create({})