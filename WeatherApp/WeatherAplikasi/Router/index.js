import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";
import { FontAwesome } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { Text } from 'react-native'

import Welcome from '../Pages/Welcome'
import RegisterScreen from '../Pages/RegisterScreen'
import Sukses from '../Pages/Sukses'
import Login from '../Pages/LoginScreen'
import Forgot from '../Pages/Forgot'
import HomeScreen from '../Pages/HomeScreen'
import Details from '../Pages/Details'
import SettingScreen from '../Pages/SettingScreen'
import TentangSaya from '../Pages/TentangSaya'
import AddScreen from '../Pages/AddScreen'
import Profil from '../Pages/Profil'

const Tab = createBottomTabNavigator()
const Drawer = createDrawerNavigator()
const Stack = createStackNavigator()

export default function Router() {
	return (
		<NavigationContainer>
			<Stack.Navigator>
				<Stack.Screen options={{headerShown:false}} name= "Welcome" component={Welcome}/>
				<Stack.Screen options={{headerShown:false}} name= "Daftar" component={RegisterScreen} />
				<Stack.Screen options={{headerShown:false}} name= "Sukses" component={Sukses} />
				<Stack.Screen options={{headerShown:false}} name= "Login" component={Login}/>
				<Stack.Screen options={{headerShown:false}} name= "Forgot" component={Forgot}/>
				<Stack.Screen name= "HomeScreen" component={HomeScreen}/>
				<Stack.Screen options={{headerShown:false}} name= "MainApp" component={MainApp}/>
				<Stack.Screen options={{headerShown:false}} name= "MyDrawer" component={MyDrawer}/>
			</Stack.Navigator>
		</NavigationContainer>
	) 	
}

const MainApp =()=>(
		<Tab.Navigator screenOptions={({route})=>({
			tabBarIcon: ({ color }) => {
			let icon
			if (route.name === 'Beranda') {
				icon = <FontAwesome name="home" size={24} color="black" /> 
			} else if(route.name === 'Details'){
				icon = "D"
			} else if(route.name === 'Add') {
				icon = "A"
			}else if(route.name === 'Profil') {
				icon = <AntDesign name="profile" size={24} color="black" />
			}
			 return <Text style={{color:color}}>{icon}</Text>
			},
		})}
			tabBarOptions={{
				activeTintColor: 'tomato',
				inactiveTintColor: 'gray',
			}}>
			<Tab.Screen options={{headerShown:false}} name= "Beranda" component={HomeScreen}/>
			<Tab.Screen name= "Details" component={Details}/>
			<Tab.Screen name= "Add" component={AddScreen}/>
			<Tab.Screen name= "Profil" component={Profil}/>
		</Tab.Navigator>
)

const MyDrawer =()=>(
		<Drawer.Navigator>
			<Drawer.Screen options={{headerShown:false}} name= "App" component={MainApp}/>
			<Drawer.Screen options={{headerShown:false}} name= "Setting" component={SettingScreen}/>
			<Drawer.Screen options={{headerShown:false}} name= "About" component={TentangSaya}/>
		</Drawer.Navigator>
)