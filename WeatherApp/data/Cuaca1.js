import React from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import { FontAwesome5, MaterialCommunityIcons, Entypo } from '@expo/vector-icons'

export default class Cuaca1 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      city: ' ',
      forecast: {
        main: ' ',
        description: ' ',
        humadity: ' ',
        pressure: ' ',
        clouds: ' ',
        wind: ' ',
        weather: ' ',
        temp: 0
      }
    };
  }
  getWeather = () => {
    let url = 'http://api.openweathermap.org/data/2.5/weather?q=' + this.state.city + '&appid=24646f5e7d8b274514ab867de6398f31&units=metric';
    return fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
        this.setState({
          forecast: {
            main: responseJson.weather[0].main,
            temp: responseJson.main.temp,
            humadity: responseJson.main.humidity,
            pressure: responseJson.main.pressure,
            clouds: responseJson.clouds.all,
            wind: responseJson.wind.speed,
            weather: responseJson.weather.icon,
          }
        });
      });
  }
  render() {
    
    return (
      <View style={styles.containerMain}>
        <View style={{ height: 24 }}></View>
        <View style={styles.Isi}>
          <View style={styles.CityInput}>
          <View style={styles.Title}>
            <Text>Enter City Name</Text>
          </View>
            <TextInput
              style={styles.InputText}
              placeholder="Enter City Name"
              onChangeText={(city) => this.setState({ city })}/>
            <Button
              onPress={() => this.getWeather()}
              title="Find"
              accessibilityLabel="Find"
            />
          </View>

          <View style={styles.Cek}>
          <View>
            <Text>{this.state.city}</Text>
          </View>
          <View style={{fontSize:30}}>
            <Text>{this.state.forecast.temp} °C </Text>
          </View>
          <Text>{this.state.forecast.city}</Text>
              <View style={styles.weatherDetails}>
                <View style={styles.weatherDetailsRow}>
                  <View style={styles.weatherDetailsBox}>
                    <View style={styles.weatherDetailsRow}>
                        <MaterialCommunityIcons name="water" size={30} color={'blue'} />
                        <View style={styles.weatherDetailsItems}>
                            <Text style={styles.textKeterangan}>Humidity</Text>
                            <Text style={styles.textSecondary}>{this.state.forecast.humadity} %</Text>
                        </View>
                    </View>
                  </View>
                  <View style={styles.weatherDetailsBox}>
                    <View style={styles.weatherDetailsRow}>
                        <MaterialCommunityIcons name="speedometer" size={30} color={'blue'} /> 
                        <View style={styles.weatherDetailsItems}>
                            <Text style={styles.textKeterangan}>Pressure</Text>
                            <Text style={styles.textSecondary}>{this.state.forecast.pressure} hPa</Text>
                        </View>
                    </View>
                  </View>
                  <View style={styles.weatherDetailsBox}>
                    <View style={styles.weatherDetailsRow}>
                    <Entypo name="cloud" size={25} color={'blue'} />
                        <View style={styles.weatherDetailsItems}>
                            <Text style={styles.textKeterangan}>Cloudiness</Text>
                            <Text style={styles.textSecondary}>{this.state.forecast.clouds} %</Text>
                        </View>
                    </View>
                  </View>
                  <View style={styles.weatherDetailsBox}>
                    <View style={styles.weatherDetailsRow}>
                        <MaterialCommunityIcons name="weather-windy" size={30} color={'blue'} />
                        <View style={styles.weatherDetailsItems}>
                            <Text style={styles.textKeterangan}>Wind</Text>
                            <Text style={styles.textSecondary}>{this.state.forecast.wind} m/s</Text>
                        </View>
                    </View>
                  </View>  
            </View>
          </View>
          </View>
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  containerMain: {
    backgroundColor: '#E3F2FD',
    flex: 1,
    flexDirection: 'column'
  },

  Isi:{
    backgroundColor: '#E3F2FD',
    flex: 8,
    top: -30
  },

  CityInput: {
    backgroundColor: '#FFFFFF',
    flex: 2,
    margin: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#EEEEEE',
    borderWidth: 1,
    borderRadius: 5,
  },

  Title: {
    color: '#000000',
    fontSize: 18,
  },

  InputText: {
    backgroundColor: '#FFFFFF',
    width: 200,
    padding: 10,
    margin: 10,
    borderColor: '#B2B2B2',
    borderWidth: 1,
    borderRadius: 5,
  },

  Cek: {
    backgroundColor: '#FAFAFA',
    flex: 4,
    margin: 10,
    marginTop:5,
    borderColor: '#EEEEEE',
    borderWidth: 1,
    borderRadius: 5,
  },

  CekText: {
    color: '#000000',
    fontSize: 18,
  },

  weatherDetails: {
    marginTop: 'auto',
    margin: 15,
    borderWidth: 1,
    width: 340,
    borderColor: 'lightgrey',
    borderRadius: 10,
    backgroundColor: '#ffffff',
    top: -200,
    left: -15
  },

  weatherDetailsRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  weatherDetailsBox: {
    flex: 1,
    padding: 25,
    marginBottom: 0,
    top: -15,
  },

  weatherDetailsItems: {
    flexDirection: 'row',
    alignSelf:'flex-start',
    top: 40,
    left: -55,
    width:100,
  },

  textSecondary: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    fontSize: 13,
    color: 'black',
    fontWeight: '700',
    top:-15,
    left: -25,
    width:50,
  },

  textKeterangan: {
    flexDirection: 'row',
    fontSize: 13,
    left: 15,
  }
});