const City=[
{
    id     : 1101,
    idProv : 11,
    Kota   : "Kabupaten Simeulue",
},
{       
    id     : 1102,
    idProv : 11,
    Kota   : "Kabupaten Aceh Singkil",
},
{
    id     : 1103,
    idProv : 11,
    Kota   : "Kabupaten Aceh Selatan",
},
{
    id     : 1104,
    idProv : 11,
    Kota   : "Kabupaten Aceh Tenggara",
},
{
    id     : 1105,
    idProv : 11,
    Kota   : "Kabupaten Aceh Timur",
},
{
    id     : 1106,
    idProv : 11,
    Kota   : "Kabupaten Aceh Tengah",
},
{
    id     : 1107,
    idProv : 11,
    Kota   : "Kabupaten Aceh Barat",
},
{
    id     : 1108,
    idProv : 11,
    Kota   : "Kabupaten Aceh Besar",
},   
{
    id     : 1109,
    idProv : 11,
    Kota   : "Kabupaten Pidie",
},   
{
    id     : 1110,
    idProv : 11,
    Kota   : "Kabupaten Bireuen",
},   
{
    id     : 1111,
    idProv : 11,
    Kota   : "Kabupaten Aceh Utara",
},   
{
    id     : 1112,
    idProv : 11,
    Kota   : "Kabupaten Aceh Barat Daya",
},   
{
    id     : 1113,
    idProv : 11,
    Kota   : "Kabupaten Gayo Lues",
},   
{
    id     : 1114,
    idProv : 11,
    Kota   : "Kabupaten Aceh Tamiang",
},
{
    id     : 1115,
    idProv : 11,
    Kota   : "Kabupaten Nagan Raya",
},
{
    id     : 1116,
    idProv : 11,
    Kota   : "Kabupaten Aceh Jaya",
},
{
    id     : 1117,
    idProv : 11,
    Kota   : "Kabupaten Bener Meriah",
},
{
    id     : 1118,
    idProv : 11,
    Kota   : "Kabupaten Pidie Jaya",
},
{
    id     : 1171,
    idProv : 11,
    Kota   : "Kota Banda Aceh"
},
{
    id     : 1172,
    idProv : 11,
    Kota   : "Kota Sabang",
},
{
    id     : 1173,
    idProv : 11,
    Kota   : "Kota Langsa"
},
{

    id     : 1174,
    idProv : 11,
    Kota   : "Kota Lhokseumawe"
},
{
    id     : 1175,
    idProv : 11,
    Kota   : "Kota Subulussalam",
},{

    id     : 1201,
    idProv : 12,
    Kota   : "Kabupaten Nias",
},
{
    id     : 1202,
    idProv : 12,
    Kota   : "Kabupaten Mandailing Natal",
},
{

    id     : 1203,
    idProv : 12,
    Kota   : "Kabupaten Tapanuli Selatan",
},
{
    id     : 1204,
    idProv : 12,
    Kota   : "Kabupaten Tapanuli Tengah",
},
{
    id     : 1205,
    idProv : 12,
    Kota   : "Kabupaten Tapanuli Utara",
},
{

    id     : 1206,
    idProv : 12,
    Kota   : "Kabupaten Toba Samosir",
},
{
    id     : 1207,
    idProv : 12,
    Kota   : "Kabupaten Labuhan Batu",

},
{
    id     : 1208,
    idProv : 12,
    Kota   : "Kabupaten Asahan",
},
{
    id     : 1209,
    idProv : 12,
    Kota   : "Kabupaten Simalungun",
},
{

    id     : 1210,
    idProv : 12,
    Kota   : "Kabupaten Dairi",
},
{
    id     : 1211,
    idProv : 12,
    Kota   : "Kabupaten Karo",
},
{
    id     : 1212,
    idProv : 12,
    Kota   : "Kabupaten Deli Serdang",
},
{
    id     : 1213,
    idProv : 12,
    Kota   : "Kabupaten Langkat",
},
{
    id     : 1214,
    idProv : 12,
    Kota   : "Kabupaten Nias Selatan",
},
{
    id     : 1215,
    idProv : 12,
    Kota   : "Kabupaten Humbang Hasundutan",

},
{
    id     : 1216,
    idProv : 12,
    Kota   : "Kabupaten Pakpak Bharat",
},
{
    id     : 1217,
    idProv : 12,
    Kota   : "Kabupaten Samosir",
},
{
    id     : 1218,
    idProv : 12,
    Kota   : "Kabupaten Serdang Bagasi",
},
{
    id     : 1219,
    idProv : 12,
    Kota   : "Kabupaten Batu Bara",
},
{
    id     : 1220,
    idProv : 12,
    Kota   : "Kabupaten Padang Lawas Utara",
},
{
    id     : 1221,
    idProv : 12,
    Kota   : "Kabupaten Padang Lawas",

},{
    id     : 1222,
    idProv : 12,
    Kota   : "Kabupaten Labuhan Batu Selatan",
},
{
    id     : 1223,
    idProv : 12,
    Kota   : "Kabupaten Labuhan Batu Utara",
},
{
    id     : 1224,
    idProv : 12,
    Kota   : "Kabupaten Nias Utara",
},
{
    id     : 1225,
    idProv : 12,
    Kota   : "Kabupaten Nias Barat",
},
{
    id     : 1271,
    idProv : 12,
    Kota   : "Kota Sibolga",
},
{
    id     : 1272,
    idProv : 12,
    Kota   : "Kota Tanjung Balai",
},
{
    id     : 1273,
    idProv : 12,
    Kota   : "Kota Pematang Siantar",
},
{
    id     : 1274,
    idProv : 12,
    Kota   : "Kota Tebing Tinggi",
},
{
    id     : 1275,
    idProv : 12,
    Kota   : "Kota Medan",
},
{
    id     : 1276,
    idProv : 12,
    Kota   : "Kota Binjai",
},
{
    id     : 1276,
    idProv : 12,
    Kota   : "Kota Padang Sidempuan",
},
{
    id     : 1278,
    idProv : 12,
    Kota   : "Kota Gunung Sitoli",
},
{
    id     : 1301,
    idProv : 13,
    Kota   : "Kabupaten Kepulauan Mentawai",
},
{
    id     : 1302,
    idProv : 13,
    Kota   : "Kabupaten Pesisir Selatan",
},
{
    id     : 1303,
    idProv : 13,
    Kota   : "Kabupaten Solok",
},
{
    id     : 1304,
    idProv : 13,
    Kota   : "Kabupaten Sijunjung",
},
{
    id     : 1305,
    idProv : 13,
    Kota   : "Kabupaten Tanah Datar",
},
{
    id     : 1306,
    idProv : 13,
    Kota   : "Kabupaten Padang Pariaman",
},
{
    id     : 1307,
    idProv : 13,
    Kota   : "Kabupaten Agam",
},
{
    id     : 1308,
    idProv : 13,
    Kota   : "Kabupaten Lima Puluh Kota",
},
{
    id     : 1309,
    idProv : 13,
    Kota   : "Kabupaten Pasaman",
},
{
    id     : 1310,
    idProv : 13,
    Kota   : "Kabupaten Solok Selatan",
},
{
    id     : 1311,
    idProv : 13,
    Kota   : "Kabupaten Dhamasraya",
},
{
    id     : 1312,
    idProv : 13,
    Kota   : "Kabupaten Pasaman Barat",
},
{
    id     : 1371,
    idProv : 13,
    Kota   : "Kota Padang",
},
{
    id     : 1372,
    idProv : 13,
    Kota   : "Kota Solok",
},
{
    id     : 1373,
    idProv : 13,
    Kota   : "Kota Sawah Lunto",
},
{
    id     : 1374,
    idProv : 13,
    Kota   : "Kota Padang Panjang",
},
{
    id     : 1375,
    idProv : 13,
    Kota   : "Kota Bukittinggi",
},
{
    id     : 1376,
    idProv : 13,
    Kota   : "Kota Payakumbuh",
},
{
    id     : 1377,
    idProv : 13,
    Kota   : "Kota Pariaman",
},
{
    id     : 1401,
    idProv : 14,
    Kota   : "Kabupaten Kuantan Singingi",
},
{
    id     : 1402,
    idProv : 14,
    Kota   : "Kabupaten Indrigiri Hulu",
},{
    id     : 1403,
    idProv : 14,
    Kota   : "Kabupaten Indrigiri Hilir",
},{
    id     : 1404,
    idProv : 14,
    Kota   : "Kabupaten Pelalawan",
},{
    id     : 1405,
    idProv : 14,
    Kota   : "Kabupaten Siak",
},{
    id     : 1406,
    idProv : 14,
    Kota   : "Kabupaten Kampar",
},{
    id     : 1407,
    idProv : 14,
    Kota   : "Kabupaten Rokan Hulu",
},{
    id     : 1408,
    idProv : 14,
    Kota   : "Kabupaten Bengkalis",
},{
    id     : 1409,
    idProv : 14,
    Kota   : "Kabupaten Rokan Hilir",
},{
    id     : 1410,
    idProv : 14,
    Kota   : "Kabupaten Kepulauan Meranti",
},
{
    id     : 1471,
    idProv : 14,
    Kota   : "Kota Pekanbaru",
},
{
    id     : 1473,
    idProv : 14,
    Kota   : "Kota Dumai",
},
{
    id     : 1501,
    idProv : 15,
    Kota   : "Kabupaten Kerinci",
},
{
    id     : 1502,
    idProv : 15,
    Kota   : "Kabupaten Merangin",
},{
    id     : 1503,
    idProv : 15,
    Kota   : "Kabupaten Sarolangun",
},{
    id     : 1504,
    idProv : 15,
    Kota   : "Kabupaten Batang Hari",
},{
    id     : 1505,
    idProv : 15,
    Kota   : "Kabupaten Muaro Jambi",
},{
    id     : 1506,
    idProv : 15,
    Kota   : "Kabupaten Tanjung Jabung Timur",
},{
    id     : 1507,
    idProv : 15,
    Kota   : "Kabupaten Tanjung Jabung Barat",
},{
    id     : 1508,
    idProv : 15,
    Kota   : "Kabupaten Tebo",
},{
    id     : 1509,
    idProv : 15,
    Kota   : "Kabupaten Bungo",
},
{
    id     : 1571,
    idProv : 15,
    Kota   : "Kota Jambi",
},{
    id     : 1572,
    idProv : 15,
    Kota   : "Kota Sungai Penuh",
},
{
    id     : 1601,
    idProv : 16,
    Kota   : "Kabupaten Ogan Komering Ulu",
},
{
    id     : 1602,
    idProv : 16,
    Kota   : "Kabupaten Ogan Komering Ilir",
},
{
    id     : 1603,
    idProv : 16,
    Kota   : "Kabupaten Ogan Muara Enim",
},
{
    id     : 1604,
    idProv : 16,
    Kota   : "Kabupaten Lahat",
},
{
    id     : 1605,
    idProv : 16,
    Kota   : "Kabupaten Musi Rawas",
},
{
    id     : 1606,
    idProv : 16,
    Kota   : "Kabupaten Musi Banyuasin",
},
{
    id     : 1607,
    idProv : 16,
    Kota   : "Kabupaten Banyu Asin",
},
{
    id     : 1608,
    idProv : 16,
    Kota   : "Kabupaten Ogan Komering Ulu Selatan",
},
{
    id     : 1609,
    idProv : 16,
    Kota   : "Kabupaten Ogan Komering Timur",
},
{
    id     : 1610,
    idProv : 16,
    Kota   : "Kabupaten Ogan Ilir",
},
{
    id     : 1611,
    idProv : 16,
    Kota   : "Kabupaten Empat Lawang",
},
{
    id     : 1612,
    idProv : 16,
    Kota   : "Kabupaten Penukal Arab Lematang Ilir",
},
{
    id     : 1613,
    idProv : 16,
    Kota   : "Kabupaten Musi Rawas Utara",
},
{
    id     : 1671,
    idProv : 16,
    Kota   : "Kota Palembang",
},
{
    id     : 1672,
    idProv : 16,
    Kota   : "Kota Prabumulih",
},
{
    id     : 1672,
    idProv : 16,
    Kota   : "Kota Pagar Alam",
},
{
    id     : 1672,
    idProv : 16,
    Kota   : "Kota Lubuklinggau",
},
{
    id     : 1701,
    idProv : 17,
    Kota   : "Kabupaten Bengkulu Selatan",
},
{
    id     : 1702,
    idProv : 17,
    Kota   : "Kabupaten Rejang Lebong",
},
{
    id     : 1703,
    idProv : 17,
    Kota   : "Kabupaten Bengkulu Utara",
},
{
    id     : 1704,
    idProv : 17,
    Kota   : "Kabupaten Kaur",
},
{
    id     : 1705,
    idProv : 17,
    Kota   : "Kabupaten Seluma",
},
{
    id     : 1706,
    idProv : 17,
    Kota   : "Kabupaten Mukomuko",
},
{
    id     : 1707,
    idProv : 17,
    Kota   : "Kabupaten Lebong",
},
{
    id     : 1708,
    idProv : 17,
    Kota   : "Kabupaten Kepahiang",
},
{
    id     : 1709,
    idProv : 17,
    Kota   : "Kabupaten Bengkulu Tengah",
},
{
    id     : 1771,
    idProv : 17,
    Kota   : "Kota Bengkulu",
},
{
    id     : 1801,
    idProv : 18,
    Kota   : "Kabupaten Lampung Barat",
},
{
    id     : 1802,
    idProv : 18,
    Kota   : "Kabupaten Tanggamus",
},
{
    id     : 1803,
    idProv : 18,
    Kota   : "Kabupaten Lampung Selatan",
},
{
    id     : 1804,
    idProv : 18,
    Kota   : "Kabupaten Lampung Timur",
},
{
    id     : 1805,
    idProv : 18,
    Kota   : "Kabupaten Lampung Tengah",
},
{
    id     : 1806,
    idProv : 18,
    Kota   : "Kabupaten Lampung Utara",
},
{
    id     : 1807,
    idProv : 18,
    Kota   : "Kabupaten Way Kanan",
},
{
    id     : 1808,
    idProv : 18,
    Kota   : "Kabupaten Tulang Bawang",
},
{
    id     : 1809,
    idProv : 18,
    Kota   : "Kabupaten Pesawaran",
},
{
    id     : 1810,
    idProv : 18,
    Kota   : "Kabupaten PringSewu",
},
{
    id     : 1811,
    idProv : 18,
    Kota   : "Kabupaten Mesuji",
},
{
    id     : 1812,
    idProv : 18,
    Kota   : "Kabupaten Tulag Bawang Barat",
},
{
    id     : 1813,
    idProv : 18,
    Kota   : "Kabupaten Pesisir Barat",
},
{
    id     : 1871,
    idProv : 18,
    Kota   : "Kota Bandar Lampung",
},
{
    id     : 1872,
    idProv : 18,
    Kota   : "Kota Metro",
},
{
    id     : 1901,
    idProv : 19,
    Kota   : "Kabupaten Bangka",
},
{
    id     : 1902,
    idProv : 19,
    Kota   : "Kabupaten Belitung",
},
{
    id     : 1903,
    idProv : 19,
    Kota   : "Kabupaten Bangka Barat",
},
{
    id     : 1904,
    idProv : 19,
    Kota   : "Kabupaten Bangka Tengah",
},
{
    id     : 1905,
    idProv : 19,
    Kota   : "Kabupaten Bangka Selatan",
},
{
    id     : 1906,
    idProv : 19,
    Kota   : "Kabupaten Belitung Timur",
},
{
    id     : 1971,
    idProv : 19,
    Kota   : "Kota Pangka Pinang",
},
{
    id     : 2101,
    idProv : 21,
    Kota   : "Kabupaten Karimun",
},
{
    id     : 2102,
    idProv : 21,
    Kota   : "Kabupaten Bintan",
},
{
    id     : 2103,
    idProv : 21,
    Kota   : "Kabupaten Natuna",
},
{
    id     : 2104,
    idProv : 21,
    Kota   : "Kabupaten Lingga",
},
{
    id     : 2105,
    idProv : 21,
    Kota   : "Kabupaten Kepulauan Anambas",
},
{
    id     : 2171,
    idProv : 21,
    Kota   : "Kota Batam",
},
{
    id     : 2172,
    idProv : 21,
    Kota   : "Kota Tanjung Pinang",
},
{
    id     : 3101,
    idProv : 31,
    Kota   : "Kabupaten Kepulauan Seribu",
},
{
    id     : 3171,
    idProv : 31,
    Kota   : "Kota Jakarta Selatan",
},
{
    id     : 3172,
    idProv : 31,
    Kota   : "Kota Jakarta Timur",
},
{
    id     : 3173,
    idProv : 31,
    Kota   : "Kota Jakarta Pusat",
},
{
    id     : 3174,
    idProv : 31,
    Kota   : "Kota Jakarta Barat",
},
{
    id     : 3175,
    idProv : 31,
    Kota   : "Kota Jakarta Utara",
},
{
    id     : 3201,
    idProv : 32,
    Kota   : "Kabupaten Bogor",
},
{
    id     : 3202,
    idProv : 32,
    Kota   : "Kabupaten Sukabumi",
},
{
    id     : 3203,
    idProv : 32,
    Kota   : "Kabupaten Cianjur",
},
{
    id     : 3204,
    idProv : 32,
    Kota   : "Kabupaten Bandung",
},
{
    id     : 3205,
    idProv : 32,
    Kota   : "Kabupaten Garut",
},
{
    id     : 3206,
    idProv : 32,
    Kota   : "Kabupaten Tasikmalaya",
},
{
    id     : 3207,
    idProv : 32,
    Kota   : "Kabupaten Ciamis",
},
{
    id     : 3208,
    idProv : 32,
    Kota   : "Kabupaten Kuningan",
},
{
    id     : 3209,
    idProv : 32,
    Kota   : "Kabupaten Cirebon",
},
{
    id     : 3210,
    idProv : 32,
    Kota   : "Kabupaten Majalengka",
},
{
    id     : 3211,
    idProv : 32,
    Kota   : "Kabupaten Sumedang",
},
{
    id     : 3212,
    idProv : 32,
    Kota   : "Kabupaten Indramayu",
},
{
    id     : 3213,
    idProv : 32,
    Kota   : "Kabupaten Subang",
},
{
    id     : 3214,
    idProv : 32,
    Kota   : "Kabupaten Purwarta",
},
{
    id     : 3215,
    idProv : 32,
    Kota   : "Kabupaten Karawang",
},
{
    id     : 3216,
    idProv : 32,
    Kota   : "Kabupaten Bekasi",
},
{
    id     : 3217,
    idProv : 32,
    Kota   : "Kabupaten Bandung Barat",
},
{
    id     : 3218,
    idProv : 32,
    Kota   : "Kabupaten Pangandaran",
},
{
    id     : 3271,
    idProv : 32,
    Kota   : "Kota Bogor",
},
{
    id     : 3272,
    idProv : 32,
    Kota   : "Kota Sukabumi",
},
{ 
    id     : 3273, 
    idProv : 32,
    Kota   : "Kota Bandung",
},
{
    id     : 3274,
    idProv : 32,
    Kota   : "Kota Cirebon",
},
{
    id     : 3275,
    idProv : 32,
    Kota   : "Kota Bekasi",
},
{
    id     : 3276,
    idProv : 32,
    Kota   : "Kota Depok",
},
{
    id     : 3277,
    idProv : 32,
    Kota   : "Kota Cimahi",
},
{
    id     : 3278,
    idProv : 32,
    Kota   : "Kota Tasikmalaya",
},
{
    id     : 3279,
    idProv : 32,
    Kota   : "Kota Banjar",
},
{
    id     : 3301,
    idProv : 33,
    Kota   : "Kabupaten Cilacap",
},
{
    id     : 3302,
    idProv : 33,
    Kota   : "Kabupaten Banyumas",
},
{
    id     : 3303,
    idProv : 33,
    Kota   : "Kabupaten Purbalingga",
},
{
    id     : 3304,
    idProv : 33,
    Kota   : "Kabupaten Banjar Negara",
},
{
    id     : 3305,
    idProv : 33,
    Kota   : "Kabupaten Kebumen",
},
{
    id     : 3306,
    idProv : 33,
    Kota   : "Kabupaten Purworejo",
},
{
    id     : 3307,
    idProv : 33,
    Kota   : "Kabupaten Wonosobo",
},
{
    id     : 3308,
    idProv : 33,
    Kota   : "Kabupaten Magelang",
},
{
    id     : 3309,
    idProv : 33,
    Kota   : "Kabupaten Boyolali",
},
{
    id     : 3310,
    idProv : 33,
    Kota   : "Kabupaten Klaten",
},
{
    id     : 3311,
    idProv : 33,
    Kota   : "Kabupaten Sukoharjo",
},
{
    id     : 3312,
    idProv : 33,
    Kota   : "Kabupaten Wonogirri",
},
{
    id     : 3313,
    idProv : 33,
    Kota   : "Kabupaten Karanganyar",
},
{
    id     : 3314,
    idProv : 33,
    Kota   : "Kabupaten Sragen",
},
{
    id     : 3315,
    idProv : 33,
    Kota   : "Kabupaten Grobogan",
},
{
    id     : 3316,
    idProv : 33,
    Kota   : "Kabupaten Blora",
},
{
    id     : 3317,
    idProv : 33,
    Kota   : "Kabupaten Rembang",
},
{
    id     : 3318,
    idProv : 33,
    Kota   : "Kabupaten Pati",
},
{
    id     : 3319,
    idProv : 33,
    Kota   : "Kabupaten Kudus",
},
{
    id     : 3320,
    idProv : 33,
    Kota   : "Kabupaten Jepara",
},
{
    id     : 3321,
    idProv : 33,
    Kota   : "Kabupaten Demak",
},
{
    id     : 3322,
    idProv : 33,
    Kota   : "Kabupaten Semarang",
},
{
    id     : 3323,
    idProv : 33,
    Kota   : "Kabupaten Temanggung",
},
{
    id     : 3324,
    idProv : 33,
    Kota   : "Kabupaten Kendal",
},
{
    id     : 3325,
    idProv : 33,
    Kota   : "Kabupaten Batang",
},
{
    id     : 3326,
    idProv : 33,
    Kota   : "Kabupaten Pekalongan",
},
{
    id     : 3327,
    idProv : 33,
    Kota   : "Kabupaten Pemalang",
},
{
    id     : 3328,
    idProv : 33,
    Kota   : "Kabupaten Tegal",
},
{
    id     : 3329,
    idProv : 33,
    Kota   : "Kabupaten Brebes",
},
{
    id     : 3371,
    idProv : 33,
    Kota   : "Kota Magelang",
},
{
    id     : 3372,
    idProv : 33,
    Kota   : "Kota Surakarta",
},
{
    id     : 3373,
    idProv : 33,
    Kota   : "Kota Salatiga",
},
{
    id     : 3374,
    idProv : 33,
    Kota   : "Kota Semarang",
},
{
    id     : 3375,
    idProv : 33,
    Kota   : "Kota Pekalongan",
},
{
    id     : 3376,
    idProv : 33,
    Kota   : "Kota Tegal",
},
{
    id     : 3401,
    idProv : 34,
    Kota   : "Kabupaten Kulon Progo",
},
{
    id     : 3402,
    idProv : 34,
    Kota   : "Kabupaten Bantul",
},
{
    id     : 3403,
    idProv : 34,
    Kota   : "Kabupaten Gunung Kidul",
},
{
    id     : 3404,
    idProv : 34,
    Kota   : "Kabupaten Sleman",
},
{
    id     : 3471,
    idProv : 34,
    Kota   : "Kota Yogyakarta",
},
{
    id     : 3501,
    idProv : 35,
    Kota   : "Kabupaten Pacitan",
},
{
    id     : 3502,
    idProv : 35,
    Kota   : "Kabupaten Ponorogo",
},
{
    id     : 3503,
    idProv : 35,
    Kota   : "Kabupaten Trenggalek",
},
{
    id     : 3504,
    idProv : 35,
    Kota   : "Kabupaten Tulungagung",
},
{
    id     : 3505,
    idProv : 35,
    Kota   : "Kabupaten Blitar",
},
{
    id     : 3506,
    idProv : 35,
    Kota   : "Kabupaten Kediri",
},
{
    id     : 3507,
    idProv : 35,
    Kota   : "Kabupaten Malang",
},
{
    id     : 3508,
    idProv : 35,
    Kota   : "Kabupaten Lumajang",
},
{
    id     : 3509,
    idProv : 35,
    Kota   : "Kabupaten Jember",
},
{
    id     : 3510,
    idProv : 35,
    Kota   : "Kabupaten Jember",
},
{
    id     : 3511,
    idProv : 35,
    Kota   : "Kabupaten Bondowoso",
},
{
    id     : 3512,
    idProv : 35,
    Kota   : "Kabupaten Situbondo",
},
{
    id     : 3513,
    idProv : 35,
    Kota   : "Kabupaten Probolinggo",
},
{
    id     : 3514,
    idProv : 35,
    Kota   : "Kabupaten Pasuruan",
},
{
    id     : 3515,
    idProv : 35,
    Kota   : "Kabupaten Sidoarjo",
},
{
    id     : 3516,
    idProv : 35,
    Kota   : "Kabupaten Mojokerto",
},
{
    id     : 3517,
    idProv : 35,
    Kota   : "Kabupaten Jombang",
},
{
    id     : 3518,
    idProv : 35,
    Kota   : "Kabupaten Nganjuk",
},
{
    id     : 3519,
    idProv : 35,
    Kota   : "Kabupaten Madiun",
},
{
    id     : 3520,
    idProv : 35,
    Kota   : "Kabupaten Magetan",
},
{
    id     : 3521,
    idProv : 35,
    Kota   : "Kabupaten Ngawi",
},
{
    id     : 3522,
    idProv : 35,
    Kota   : "Kabupaten Bojonegoro",
},
{
    id     : 3523,
    idProv : 35,
    Kota   : "Kabupaten Tuban",
},
{
    id     : 3524,
    idProv : 35,
    Kota   : "Kabupaten Lamongan",
},
{
    id     : 3525,
    idProv : 35,
    Kota   : "Kabupaten Gresik",
},
{
    id     : 3526,
    idProv : 35,
    Kota   : "Kabupaten Bangkalan",
},
{
    id     : 3527,
    idProv : 35,
    Kota   : "Kabupaten Sampang",
},
{
    id     : 3528,
    idProv : 35,
    Kota   : "Kabupaten Pamekasan",
},
{
    id     : 3529,
    idProv : 35,
    Kota   : "Kabupaten Sumenep",
},
{
    id     : 3571,
    idProv : 35,
    Kota   : "Kota Kediri",
},
{
    id     : 3572,
    idProv : 35,
    Kota   : "Kota Blitar",
},
{
    id     : 3573,
    idProv : 35,
    Kota   : "Kota Malang",
},
{
    id     : 3574,
    idProv : 35,
    Kota   : "Kota Probolinggo",
},
{
    id     : 3575,
    idProv : 35,
    Kota   : "Kota Pasuruan",
},
{
    id     : 3576,
    idProv : 35,
    Kota   : "Kota Mojokerto",
},
{
    id     : 3577,
    idProv : 35,
    Kota   : "Kota Madiun",
},
{
    id     : 3578,
    idProv : 35,
    Kota   : "Kota Surabaya",
},
{
    id     : 3579,
    idProv : 35,
    Kota   : "Kota Batu",
},
{
    id     : 3601,
    idProv : 36,
    Kota   : "Kabupaten Pandeglang",
},
{
    id     : 3602,
    idProv : 36,
    Kota   : "Kabupaten Lebak",
},
{
    id     : 3603,
    idProv : 36,
    Kota   : "Kabupaten Tangerang",
},
{
    id     : 3604,
    idProv : 36,
    Kota   : "Kabupaten Serang",
},
{
    id     : 3671,
    idProv : 36,
    Kota   : "Kota Tangerang"
},
{
    id     : 3672,
    idProv : 36,
    Kota   : "Kota Cilegon"
},
{
    id     : 3673,
    idProv : 36,
    Kota   : "Kota Serang"
},
{
    id     : 3674,
    idProv : 36,
    Kota   : "Kota Tangerang Selatan"
},
{
    id     : 5101,
    idProv : 51,
    Kota   : "Kabupaten Jembrana"
},
{
    id     : 5102,
    idProv : 51,
    Kota   : "Kabupaten Tabanan"
},
{
    id     : 5103,
    idProv : 51,
    Kota   : "Kabupaten Badung"
},
{
    id     : 5104,
    idProv : 51,
    Kota   : "Kabupaten Gianyar"
},
{
    id     : 5105,
    idProv : 51,
    Kota   : "Kabupaten Klungkung"
},
{
    id     : 5106,
    idProv : 51,
    Kota   : "Kabupaten Bangli"
},
{
    id     : 5107,
    idProv : 51,
    Kota   : "Kabupaten Karang Asem"
},
{
    id     : 5108,
    idProv : 51,
    Kota   : "Kabupaten Buleleng"
},
{
    id     : 5171,
    idProv : 51,
    Kota   : "Kota Denpasar"
},
{
    id     : 5201,
    idProv : 52,
    Kota   : "Kabupaten Lombok Barat"
},
{
    id     : 5202,
    idProv : 52,
    Kota   : "Kabupaten Lombok Tengah"
},
{
    id     : 5203,
    idProv : 52,
    Kota   : "Kabupaten Lombok Timur"
},
{
    id     : 5204,
    idProv : 52,
    Kota   : "Kabupaten Sumbawa"
},
{
    id     : 5205,
    idProv : 52,
    Kota   : "Kabupaten Dompu"
},
{
    id     : 5206,
    idProv : 52,
    Kota   : "Kabupaten Bima"
},
{
    id     : 5207,
    idProv : 52,
    Kota   : "Kabupaten Sumbawa Barat"
},
{
    id     : 5208,
    idProv : 52,
    Kota   : "Kabupaten Lombok Utara"
},
{
    id     : 5271,
    idProv : 52,
    Kota   : "Kota Mataram"
},
{
    id     : 5272,
    idProv : 52,
    Kota   : "Kota Bima"
},
{
    id     : 5301,
    idProv : 53,
    Kota   : "Kabupaten Sumba Barat"
},
{
    id     : 5302,
    idProv : 53,
    Kota   : "Kabupaten Sumba Timur"
},
{
    id     : 5303,
    idProv : 53,
    Kota   : "Kabupaten Kupang"
},
{
    id     : 5304,
    idProv : 53,
    Kota   : "Kabupaten Timor Tengah Selatan"
},
{
    id     : 5305,
    idProv : 53,
    Kota   : "Kabupaten Timor Tengah Utara"
},
{
    id     : 5306,
    idProv : 53,
    Kota   : "Kabupaten Belu"
},
{
    id     : 5307,
    idProv : 53,
    Kota   : "Kabupaten Alor"
},
{
    id     : 5308,
    idProv : 53,
    Kota   : "Kabupaten Lembata"
},
{
    id     : 5309,
    idProv : 53,
    Kota   : "Kabupaten Flores Timur"
},
{
    id     : 5310,
    idProv : 53,
    Kota   : "Kabupaten Sikka"
},
{
    id     : 5311,
    idProv : 53,
    Kota   : "Kabupaten Ende"
},
{
    id     : 5312,
    idProv : 53,
    Kota   : "Kabupaten Ngada"
},
{
    id     : 5313,
    idProv : 53,
    Kota   : "Kabupaten Manggarai"
},
{
    id     : 5314,
    idProv : 53,
    Kota   : "Kabupaten Rote Ndao"
},
{
    id     : 5315,
    idProv : 53,
    Kota   : "Kabupaten Manggarai Barat"
},
{
    id     : 5316,
    idProv : 53,
    Kota   : "Kabupaten Sumba Tengah"
},
{
    id     : 5317,
    idProv : 53,
    Kota   : "Kabupaten Sumba Barat Daya"
},
{
    id     : 5318,
    idProv : 53,
    Kota   : "Kabupaten Nagekeo"
},
{
    id     : 5319,
    idProv : 53,
    Kota   : "Kabupaten Manggarai Timur"
},
{
    id     : 5320,
    idProv : 53,
    Kota   : "Kabupaten Sabu Raijua"
},
{
    id     : 5321,
    idProv : 53,
    Kota   : "Kabupaten Malaka"
},
{
    id     : 5371,
    idProv : 53,
    Kota   : "Kota Kupang"
},
{
    id     : 6101,
    idProv : 61,
    Kota   : "Kabupaten Sambas"
},
{
    id     : 6102,
    idProv : 61,
    Kota   : "Kabupaten Bengkayang"
},
{
    id     : 6103,
    idProv : 61,
    Kota   : "Kabupaten Landak"
},
{
    id     : 6104,
    idProv : 61,
    Kota   : "Kabupaten Mempawah"
},
{
    id     : 6105,
    idProv : 61,
    Kota   : "Kabupaten Sanggau"
},
{
    id     : 6106,
    idProv : 61,
    Kota   : "Kabupaten Ketapang"
},
{
    id     : 6107,
    idProv : 61,
    Kota   : "Kabupaten Sintang"
},
{
    id     : 6108,
    idProv : 61,
    Kota   : "Kabupaten Kapuas Hulu"
},
{
    id     : 6109,
    idProv : 61,
    Kota   : "Kabupaten Sekadau"
},
{
    id     : 6110,
    idProv : 61,
    Kota   : "Kabupaten Melawi"
},
{
    id     : 6111,
    idProv : 61,
    Kota   : "Kabupaten Kayong Utara"
},
{
    id     : 6112,
    idProv : 61,
    Kota   : "Kabupaten Kubu Raya"
},
{
    id     : 6171,
    idProv : 61,
    Kota   : "Kota Pontianak"
},
{
    id     : 6172,
    idProv : 61,
    Kota   : "Kota Singkawang"
},
{
    id     : 6201,
    idProv : 62,
    Kota   : "Kabupaten Kotawaringin Barat"
},
{
    id     : 6202,
    idProv : 62,
    Kota   : "Kabupaten Kotawaringin Timur"
},
{
    id     : 6203,
    idProv : 62,
    Kota   : "Kabupaten Kapuas"
},
{
    id     : 6204,
    idProv : 62,
    Kota   : "Kabupaten Barito Selatan"
},
{
    id     : 6205,
    idProv : 62,
    Kota   : "Kabupaten Barito Utara"
},
{
    id     : 6206,
    idProv : 62,
    Kota   : "Kabupaten Sukamara"
},
{
    id     : 6207,
    idProv : 62,
    Kota   : "Kabupaten Lamandau"
},
{
    id     : 6208,
    idProv : 62,
    Kota   : "Kabupaten Seruyan"
},
{
    id     : 6209,
    idProv : 62,
    Kota   : "Kabupaten Katingan"
},
{
    id     : 6210,
    idProv : 62,
    Kota   : "Kabupaten Pulang Pisau"
},
{
    id     : 6211,
    idProv : 62,
    Kota   : "Kabupaten Gunung Mas"
},
{
    id     : 6212,
    idProv : 62,
    Kota   : "Kabupaten Barito Timur"
},
{
    id     : 6213,
    idProv : 62,
    Kota   : "Kabupaten Murung Raya"
},
{
    id     : 6271,
    idProv : 62,
    Kota   : "Kota Palangkaraya"
},
{
    id     : 6301,
    idProv : 63,
    Kota   : "Kabupaten Tanah Laut"
},
{
    id     : 6302,
    idProv : 63,
    Kota   : "Kabupaten Kota Baru"
},
{
    id     : 6303,
    idProv : 63,
    Kota   : "Kabupaten Banjar"
},
{
    id     : 6304,
    idProv : 63,
    Kota   : "Kabupaten Barito Kuala"
},
{
    id     : 6305,
    idProv : 62,
    Kota   : "Kabupaten Tapin"
},
{
    id     : 6306,
    idProv : 63,
    Kota   : "Kabupaten Hulu Sungai Selatan"
},
{
    id     : 6307,
    idProv : 63,
    Kota   : "Kabupaten Hulu Sungai Tengah"
},
{
    id     : 6308,
    idProv : 63,
    Kota   : "Kabupaten Hulu Sungai Utara"
},
{
    id     : 6309,
    idProv : 63,
    Kota   : "Kabupaten Tabalong"
},
{
    id     : 6310,
    idProv : 63,
    Kota   : "Kabupaten Tanah Bumbu"
},
{
    id     : 6311,
    idProv : 63,
    Kota   : "Kabupaten Balangan"
},
{
    id     : 6371,
    idProv : 63,
    Kota   : "Kota Banjarmasin"
},
{
    id     : 6372,
    idProv : 63,
    Kota   : "Kota Banjar Baru"
},
{
    id     : 6401,
    idProv : 64,
    Kota   : "Kabupaten Paser"
},
{
    id     : 6402,
    idProv : 64,
    Kota   : "Kabupaten Kutai Barat"
},
{
    id     : 6403,
    idProv : 64,
    Kota   : "Kabupaten Kutai Kartanegara"
},
{
    id     : 6404,
    idProv : 64,
    Kota   : "Kabupaten Kutai Timur"
},
{
    id     : 6405,
    idProv : 64,
    Kota   : "Kabupaten Berau"
},
{
    id     : 6409,
    idProv : 64,
    Kota   : "Kabupaten Penajam Paser Utara"
},
{
    id     : 6411,
    idProv : 64,
    Kota   : "Kabupaten Mahakam Hulu"
},
{
    id     : 6471,
    idProv : 64,
    Kota   : "Kota Balikpapan"
},
{
    id     : 6472,
    idProv : 64,
    Kota   : "Kota Samarinda"
},
{
    id     : 6473,
    idProv : 64,
    Kota   : "Kota Bontang"
},
{
    id     : 6501,
    idProv : 65,
    Kota   : "Kabupaten Malinau"
},
{
    id     : 6502,
    idProv : 65,
    Kota   : "Kabupaten Bulungan"
},
{
    id     : 6503,
    idProv : 65,
    Kota   : "Kabupaten Tana Tidung"
},
{
    id     : 6504,
    idProv : 65,
    Kota   : "Kabupaten Nunukan"
},
{
    id     : 6571,
    idProv : 65,
    Kota   : "Kota Tarakan"
},
{
    id     : 7101,
    idProv : 71,
    Kota   : "Kabupaten Bolaang Mongondow"
},
{
    id     : 7102,
    idProv : 71,
    Kota   : "Kabupaten Minahasa"
},
{
    id     : 7103,
    idProv : 71,
    Kota   : "Kabupaten Kepulauan Sangihe"
},
{
    id     : 7104,
    idProv : 71,
    Kota   : "Kabupaten Kepulauan Talaud"
},
{
    id     : 7105,
    idProv : 71,
    Kota   : "Kabupaten Minahasa Selatan"
},
{
    id     : 7106,
    idProv : 71,
    Kota   : "Kabupaten Minahasa Utara"
},
{
    id     : 7107,
    idProv : 71,
    Kota   : "Kabupaten Bolaang Mongondow Utara"
},
{
    id     : 7108,
    idProv : 71,
    Kota   : "Kabupaten Siau Tagulandang Biaro"
},
{
    id     : 7109,
    idProv : 71,
    Kota   : "Kabupaten Minahasa Tenggara"
},
{
    id     : 7110,
    idProv : 71,
    Kota   : "Kabupaten Bolaang Mongondow Selatan"
},
{
    id     : 7111,
    idProv : 71,
    Kota   : "Kabupaten Bolaang Mongondow Timur"
},
{
    id     : 7171,
    idProv : 71,
    Kota   : "Kota Manado"
},
{
    id     : 7172,
    idProv : 71,
    Kota   : "Kota Bitung"
},
{
    id     : 7173,
    idProv : 71,
    Kota   : "Kota Tomohon"
},
{
    id     : 7174,
    idProv : 71,
    Kota   : "Kota Kotamobagu"
},
{
    id     : 7201,
    idProv : 72,
    Kota   : "Kabupaten Banggai Kepulauan"
},
{
    id     : 7202,
    idProv : 72,
    Kota   : "Kabupaten Banggai"
},
{
    id     : 7203,
    idProv : 72,
    Kota   : "Kabupaten Morowali"
},
{
    id     : 7204,
    idProv : 72,
    Kota   : "Kabupaten Poso"
},
{
    id     : 7205,
    idProv : 72,
    Kota   : "Kabupaten Donggala"
},
{
    id     : 7206,
    idProv : 72,
    Kota   : "Kabupaten Toli-Toli"
},
{
    id     : 7207,
    idProv : 72,
    Kota   : "Kabupaten Buol"
},
{
    id     : 7208,
    idProv : 72,
    Kota   : "Kabupaten Parigi Moutong"
},
{
    id     : 7209,
    idProv : 72,
    Kota   : "Kabupaten Tojo Una-Una"
},
{
    id     : 7210,
    idProv : 72,
    Kota   : "Kabupaten Sigi"
},
{
    id     : 7211,
    idProv : 72,
    Kota   : "Kabupaten Banggai Laut"
},
{
    id     : 7212,
    idProv : 72,
    Kota   : "Kabupaten Morowali Utara"
},
{
    id     : 7271,
    idProv : 72,
    Kota   : "Kota Palu"
},
{
    id     : 7301,
    idProv : 73,
    Kota   : "Kabupaten Kepulauan Selayar"
},
{
    id     : 7302,
    idProv : 73,
    Kota   : "Kabupaten Bulukumba"
},
{
    id     : 7303,
    idProv : 73,
    Kota   : "Kabupaten Bantaeng"
},
{
    id     : 7304,
    idProv : 73,
    Kota   : "Kabupaten Jeneponto"
},
{
    id     : 7305,
    idProv : 73,
    Kota   : "Kabupaten Takalar"
},
{
    id     : 7306,
    idProv : 73,
    Kota   : "Kabupaten Gowa"
},
{
    id     : 7307,
    idProv : 73,
    Kota   : "Kabupaten Sinjai"
},
{
    id     : 7308,
    idProv : 73,
    Kota   : "Kabupaten Maros"
},
{
    id     : 7309,
    idProv : 73,
    Kota   : "Kabupaten Pangkajene dan Kepulauan"
},
{
    id     : 7310,
    idProv : 73,
    Kota   : "Kabupaten Barru"
},
{
    id     : 7311,
    idProv : 73,
    Kota   : "Kabupaten Bone"
},
{
    id     : 7312,
    idProv : 73,
    Kota   : "Kabupaten Soppeng"
},
{
    id     : 7313,
    idProv : 73,
    Kota   : "Kabupaten Wajo"
},
{
    id     : 7314,
    idProv : 73,
    Kota   : "Kabupaten Sindenreng Rappang"
},
{
    id     : 7315,
    idProv : 73,
    Kota   : "Kabupaten Pinrang"
},
{
    id     : 7316,
    idProv : 73,
    Kota   : "Kabupaten Enrekang"
},
{
    id     : 7317,
    idProv : 73,
    Kota   : "Kabupaten Luwu"
},
{
    id     : 7318,
    idProv : 73,
    Kota   : "Kabupaten Tana Toraja"
},
{
    id     : 7322,
    idProv : 73,
    Kota   : "Kabupaten Luwu Utara"
},
{
    id     : 7325,
    idProv : 73,
    Kota   : "Kabupaten Luwu Timur"
},
{
    id     : 7326,
    idProv : 73,
    Kota   : "Kabupaten Toraja Utara"
},
{
    id     : 7371,
    idProv : 73,
    Kota   : "Kota Makassar"
},
{
    id     : 7371,
    idProv : 73,
    Kota   : "Kota Parepare"
},
{
    id     : 7371,
    idProv : 73,
    Kota   : "Kota Palopo"
},
{
    id     : 7401,
    idProv : 74,
    Kota   : "Kabupaten Buton"
},
{
    id     : 7402,
    idProv : 74,
    Kota   : "Kabupaten Muna"
},
{
    id     : 7403,
    idProv : 74,
    Kota   : "Kabupaten Konawe"
},
{
    id     : 7404,
    idProv : 74,
    Kota   : "Kabupaten Kolaka"
},
{
    id     : 7405,
    idProv : 74,
    Kota   : "Kabupaten Konawe Selatan"
},
{
    id     : 7406,
    idProv : 74,
    Kota   : "Kabupaten Bombana"
},
{
    id     : 7407,
    idProv : 74,
    Kota   : "Kabupaten Wakatobi"
},
{
    id     : 7408,
    idProv : 74,
    Kota   : "Kabupaten Kolaka Utara"
},
{
    id     : 7409,
    idProv : 74,
    Kota   : "Kabupaten Buton Utara"
},
{
    id     : 7410,
    idProv : 74,
    Kota   : "Kabupaten Konawe Utara"
},
{
    id     : 7411,
    idProv : 74,
    Kota   : "Kabupaten Kolaka Timur"
},
{
    id     : 7412,
    idProv : 74,
    Kota   : "Kabupaten Konawe Kepulauan  "
},
{
    id     : 7413,
    idProv : 74,
    Kota   : "Kabupaten Muna Barat"
},
{
    id     : 7414,
    idProv : 74,
    Kota   : "Kabupaten Buton Tengah"
},
{
    id     : 7415,
    idProv : 74,
    Kota   : "Kabupaten Buton Selatan"
},
{
    id     : 7471,
    idProv : 74,
    Kota   : "Kota Kendari"
},
{
    id     : 7472,
    idProv : 74,
    Kota   : "Kota Baubau"
},
{
    id     : 7501,
    idProv : 75,
    Kota   : "Kabupaten Boalemo"
},
{
    id     : 7502,
    idProv : 75,
    Kota   : "Kabupaten Gorontalo"
},
{
    id     : 7503,
    idProv : 75,
    Kota   : "Kabupaten Pohuwato"
},
{
    id     : 7504,
    idProv : 75,
    Kota   : "Kabupaten Bone Bolango"
},
{
    id     : 7505,
    idProv : 75,
    Kota   : "Kabupaten Gorontalo Utara"
},
{
    id     : 7571,
    idProv : 75,
    Kota   : "Kota Gorontalo"
},
{
    id     : 7601,
    idProv : 76,
    Kota   : "Kabupaten Majene"
},
{
    id     : 7602,
    idProv : 76,
    Kota   : "Kabupaten Polewali Mandar"
},
{
    id     : 7603,
    idProv : 76,
    Kota   : "Kabupaten Mamasa"
},
{
    id     : 7604,
    idProv : 76,
    Kota   : "Kabupaten Mamuju"
},
{
    id     : 7605,
    idProv : 76,
    Kota   : "Kabupaten Mamuju Utara"
},
{
    id     : 7606,
    idProv : 76,
    Kota   : "Kabupaten Mamuju Tengah"
},
{
    id     : 8102,
    idProv : 81,
    Kota   : "Kabupaten Maluku Tenggara"
},
{
    id     : 8103,
    idProv : 81,
    Kota   : "Kabupaten Maluku Tengah"
},
{
    id     : 8104,
    idProv : 81,
    Kota   : "Kabupaten Buru"
},
{
    id     : 8105,
    idProv : 81,
    Kota   : "Kabupaten Kepulauan Aru"
},
{
    id     : 8106,
    idProv : 81,
    Kota   : "Kabupaten Seram Bagian Barat"
},
{
    id     : 8107,
    idProv : 81,
    Kota   : "Kabupaten Seram Bagian Timur"
},
{
    id     : 8108,
    idProv : 81,
    Kota   : "Kabupaten Maluku Barat Daya"
},
{
    id     : 8109,
    idProv : 81,
    Kota   : "Kabupaten Buru Selatan"
},
{
    id     : 8171,
    idProv : 81,
    Kota   : "Kota Ambon"
},
{
    id     : 8172,
    idProv : 81,
    Kota   : "Kota Tual"
},
{
    id     : 8201,
    idProv : 82,
    Kota   : "Kabupaten Halmahera Barat"
},
{
    id     : 8202,
    idProv : 82,
    Kota   : "Kabupaten Halmahera Tengah"
},
{
    id     : 8203,
    idProv : 82,
    Kota   : "Kabupaten Kepulauan Sula"
},
{
    id     : 8204,
    idProv : 82,
    Kota   : "Kabupaten Halmahera Selatan"
},
{
    id     : 8205,
    idProv : 82,
    Kota   : "Kabupaten Halmahera Utara"
},
{
    id     : 8206,
    idProv : 82,
    Kota   : "Kabupaten Halmahera Timur"
},
{
    id     : 8207,
    idProv : 82,
    Kota   : "Kabupaten Pulau Morotai"
},
{
    id     : 8208,
    idProv : 82,
    Kota   : "Kabupaten Pulau Taliabu"
},
{
    id     : 8271,
    idProv : 82,
    Kota   : "Kota Ternate"
},
{
    id     : 8272,
    idProv : 82,
    Kota   : "Kota Tidore Kepulauan"
},
{
    id     : 9101,
    idProv : 91,
    Kota   : "Kabupaten Fakfak"
},
{
    id     : 9102,
    idProv : 91,
    Kota   : "Kabupaten Kaimana"
},
{
    id     : 9103,
    idProv : 91,
    Kota   : "Kabupaten Teluk Wondama"
},
{
    id     : 9104,
    idProv : 91,
    Kota   : "Kabupaten Teluk Bintuni"
},
{
    id     : 9105,
    idProv : 91,
    Kota   : "Kabupaten Manokwari"
},
{
    id     : 9106,
    idProv : 91,
    Kota   : "Kabupaten Sorong Selatan"
},
{
    id     : 9107,
    idProv : 91,
    Kota   : "Kabupaten Sorong"
},
{
    id     : 9108,
    idProv : 91,
    Kota   : "Kabupaten Raja Ampat"
},
{
    id     : 9109,
    idProv : 91,
    Kota   : "Kabupaten Tambrauw"
},
{
    id     : 9110,
    idProv : 91,
    Kota   : "Kabupaten Maybrat"
},
{
    id     : 9111,
    idProv : 91,
    Kota   : "Kabupaten Manokwari Selatan"
},
{
    id     : 9112,
    idProv : 91,
    Kota   : "Kabupaten Pegunungan Arfak"
},
{
    id     : 9171,
    idProv : 91,
    Kota   : "Kota Sorong"
},
{
    id     : 9401,
    idProv : 94,
    Kota   : "Kabupaten Merauke"
},
{
    id     : 9402,
    idProv : 94,
    Kota   : "Kabupaten Jayawijaya"
},
{
    id     : 9403,
    idProv : 94,
    Kota   : "Kabupaten Jayapura"
},
{
    id     : 9404,
    idProv : 94,
    Kota   : "Kabupaten Nabire"
},
{
    id     : 9408,
    idProv : 94,
    Kota   : "Kabupaten Kepulauan Yapen"
},
{
    id     : 9409,
    idProv : 94,
    Kota   : "Kabupaten Biak Numfor"
},
{
    id     : 9410,
    idProv : 94,
    Kota   : "Kabupaten Paniai"
},
{
    id     : 9411,
    idProv : 94,
    Kota   : "Kabupaten Puncak Jaya"
},
{
    id     : 9412,
    idProv : 94,
    Kota   : "Kabupaten Mimika"
},
{
    id     : 9413,
    idProv : 94,
    Kota   : "Kabupaten Boven Digoel"
},
{
    id     : 9414,
    idProv : 94,
    Kota   : "Kabupaten Mappi"
},
{
    id     : 9415,
    idProv : 94,
    Kota   : "Kabupaten Asmat"
},
{
    id     : 9416,
    idProv : 94,
    Kota   : "Kabupaten Yahukimo"
},
{
    id     : 9417,
    idProv : 94,
    Kota   : "Kabupaten Pegunungan Bintang"
},
{
    id     : 9418,
    idProv : 94,
    Kota   : "Kabupaten Tolikara"
},
{
    id     : 9419,
    idProv : 94,
    Kota   : "Kabupaten Sarmi"
},
{
    id     : 9420,
    idProv : 94,
    Kota   : "Kabupaten Keerom"
},
{
    id     : 9420,
    idProv : 94,
    Kota   : "Kabupaten Keerom"
},
{
    id     : 9426,
    idProv : 94,
    Kota   : "Kabupaten Waropen"
},
{
    id     : 9427,
    idProv : 94,
    Kota   : "Kabupaten Supiori"
},
{
    id     : 9428,
    idProv : 94,
    Kota   : "Kabupaten Mamberamo Raya"
},
{
    id     : 9429,
    idProv : 94,
    Kota   : "Kabupaten Nduga"
},
{
    id     : 9430,
    idProv : 94,
    Kota   : "Kabupaten Lanny Jaya"
},
{
    id     : 9431,
    idProv : 94,
    Kota   : "Kabupaten Mamberamo Tengah"
},
{
    id     : 9432,
    idProv : 94,
    Kota   : "Kabupaten Yalimo"
},
{
    id     : 9433,
    idProv : 94,
    Kota   : "Kabupaten Puncak"
},
{
    id     : 9434,
    idProv : 94,
    Kota   : "Kabupaten Dogiyai"
},
{
    id     : 9435,
    idProv : 94,
    Kota   : "Kabupaten Intan Jaya"
},
{
    id     : 9436,
    idProv : 94,
    Kota   : "Kabupaten Deiyai"
},
{
    id     : 9471,
    idProv : 94,
    Kota   : "Kota Jayapura"
}
]
export {Provinsi};