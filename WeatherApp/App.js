import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Cuaca1 from './data/Cuaca1'


export default function App() {
  return(
      <Cuaca1/>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
  },
  main : {
    flex: 1,
    justifyContent: "center",
  }
});